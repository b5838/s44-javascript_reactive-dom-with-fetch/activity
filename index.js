// fetch() method in JavaScript is used to send request in the server and load the received response in the webpages. The request and response is in JSON format.

// Syntax:
    // fetch('url', options)
        // url - this is the url which the request is to be made(endpoint).
        // options - array of properties that contains the HTTP method, body of request, headers.

// Mock Database
let posts = []
// Posts ID
let count = 1;

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json())
// invoke the showPosts
.then((data) => showPosts(data))

// Add post data

document.querySelector("#form-add-post")
.addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers:{
            "Content-Type": "application/json"
        }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert("Successfully added!");
    })
    
    // resets the state of our input into blanks after submitting a new post.
    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-body").value = null;

})

// View Posts
const showPosts = (posts) =>{
    let postEntries = "";

    // We will use forEach() to display each movie inside our mock database

    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit post button
// We will create a function that will be called in the onclick() event and will pass the value in the Update Form input box.
const editPost = (id) => {
    // Contain the value of the title and body in a variable.
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    // Pass the id, title, and body of the movie post to be updatedin the edit post.
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    // To remove the disable property
    document.querySelector("btn-submit-update").removeAttribute("disabled")
}

// Update post.
// This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    let id = document.querySelector("#txt-edit-id").value

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            id: id,
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers:{
            "Content-Type":"application/json"
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        alert("Successfully updated")

        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;

        // Add a attribure in a HTML element
        document.querySelector("#btn-submit-update").setAttribute("disabled",  true)
    })
})

// Activity
/* Create the function called on the “onclick” event of the delete button.
Make sure that the Now Showing List will be updated upon clicking the delete button.
Once done create a remote link and push to git with the commit message of WDC028-43.
Add the link in Boodle. 
*/

const deletePost = (id) => {
    // let movie = document.querySelector(`#post-${post.id}`).value;
    // let title = document.querySelector(`#post-title-${id}`).innerHTML;
    // let body = document.querySelector(`#post-body-${id}`).innerHTML
    // let postId = document.querySelector("#txt-edit-id").value

    for (i = 0; posts.length; i++){
        if(posts[i].id == id){
            delete posts[i];
        }
    }
}